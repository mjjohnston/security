﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Xml;

namespace mvc.Controllers
{
    public class UserController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            string KerberosId = ""
               , Ticket = Request["ticket"]
               , Service = Request.Url.GetLeftPart(UriPartial.Path)
               ;

            // This is after they've logged into CAS, it would've redirected back here with
            // a ticket.
            if (Ticket != null)
            {
                string NetId = null;
                string ValidateUrl = "https://cas.ucdavis.edu/cas/serviceValidate?ticket=" + Ticket + "&service=" + Service;
                StreamReader Reader = new StreamReader(new WebClient().OpenRead(ValidateUrl));
                string Resp = Reader.ReadToEnd();
                NameTable NT = new NameTable();
                XmlNamespaceManager NSMgr = new XmlNamespaceManager(NT);
                XmlParserContext Context = new XmlParserContext(null, NSMgr, null, XmlSpace.None);
                XmlTextReader reader = new XmlTextReader(Resp, XmlNodeType.Element, Context);

                while (reader.Read())
                {
                    if (reader.IsStartElement())
                    {
                        string Tag = reader.LocalName;
                        if (Tag == "user")
                        {
                            NetId = reader.ReadString();
                        }
                    }
                }
                reader.Close();

                if (NetId != null)
                {
                    KerberosId = NetId;
                }
            } // End if Ticket is null.
            else
            {
                Session.Add("actionUrl", Request.RawUrl);
            }

            // If there was a ticket, it would've filled in the Kerberos ID.
            if (String.Equals(KerberosId, "") == false)
            {
                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, KerberosId, DateTime.Now, DateTime.Now.AddMinutes(30), false, "App data for this user.", FormsAuthentication.FormsCookiePath);
                string encTicket = FormsAuthentication.Encrypt(ticket);
                Response.Cookies.Add(new System.Web.HttpCookie(FormsAuthentication.FormsCookieName, encTicket));
                try
                {
                    string[] sendTo = SplitUrl(Session["actionUrl"].ToString());
                    return RedirectToAction(sendTo[1], sendTo[0]);
                }
                catch
                {
                    FormsAuthentication.RedirectFromLoginPage(KerberosId, false);
                    return null;
                }
            }
            // This is when they first log into the page.
            else
            {
                string redir = "https://cas.ucdavis.edu/cas/" + "login?" + "service=" + Service;
                Response.Redirect(redir);
                return null;
            }
        }

        [HttpGet]
        public void Logout()
        {
            FormsAuthentication.SignOut();

            string redir = "https://cas.ucdavis.edu/cas/" + "logout?" + "service=" + Request.Url.GetLeftPart(UriPartial.Path);
            Response.Redirect(redir);
        }

        public string[] SplitUrl(string url)
        {
            //List<string> _return = new List<string>();

            //int _pos = url.IndexOf("%") + 3;
            //int _max = url.Length;
            //int _end = 0;
            //while (_pos < _max)
            //{
            //    _end = url.IndexOf("%", _pos);
            //    _return.Add(url.Substring(_pos, _end));
            //}

            return new string[] { "Home", "About" };
        }
    }
}