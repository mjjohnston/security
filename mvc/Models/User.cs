﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace mvc.Models
{
    public class User
    {
        [Required]
        [Display(Name = "Login Name")]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string firstname { get; set; }

        public string[] GetRoles(string loginId)
        {
            List<string> _return = new List<string>();
            string connSec = "Data Source=CRUS-ARIZONA.ou.ad3.ucdavis.edu;Initial Catalog=CRUS_Security;Integrated Security=True";
            // change this code to check for roles from the Security Database
            SqlConnection conn = new SqlConnection(connSec);
            using (conn)
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("GetUserRoles", conn);
                cmd.Parameters.Add(new SqlParameter("LoginId", System.Data.SqlDbType.VarChar)).Value = loginId;
                cmd.Parameters.Add(new SqlParameter("App", System.Data.SqlDbType.VarChar)).Value = "Permissions";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader reader = cmd.ExecuteReader();
                while(reader.Read())
                {
                    _return.Add(reader[0].ToString());
                }
            }
            return _return.ToArray();
        }
    }

    public class CRUSIdentity : IIdentity
    {
        public string Name { get; set; }

        // Affiliation would be Faculty, Student or Staff (maybe Volunteer, External, Student Employee...)
        public string Affiliation { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string HomeDepartment { get; set; }
        public string AuthenticationType
        {
            get { return "CRUS/CAS"; }
            set { throw new Exception("This attribute cannot be set."); }
        }
        public bool IsAuthenticated { get; set; }

        public CRUSIdentity(string UserName)
        {
            Name = UserName;

        }
    }

    public class CRUSPrincipal : IPrincipal
    {
        public IIdentity Identity { get; set; }

        public string[] Roles { get; set; }

        bool IPrincipal.IsInRole(string role)
        {
            throw new NotImplementedException();
        }
    }
}